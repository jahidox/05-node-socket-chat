const { Router } = require('express');
const { check } = require('express-validator');
const { get_user, post_user, put_user, patch_user, delete_user } = require('../controllers/user.controller');
const { validate_fields, validate_jwt, validate_admin_role, validate_if_has_role } = require('../middlewares/');
const { validate_role, validate_email_exists, validate_user_id_exists } = require('../helpers/db_validators');
const router = Router();

router.get('/', get_user);

router.post('/', [
  check('name', 'El nombre es obligatorio').not().isEmpty(),
  check('password', 'La contraseña edebe contener más de 6 letras').isLength({min: 6}),
  check('email', 'Correo invalido').isEmail(),
  check('email').custom(validate_email_exists),
  // check('role', 'No es un rol valido').isIn(['ADMIN_ROLE', 'USER_ROLE']),
  // check('role').custom((role) => validate_role(role)),
  check('role').custom(validate_role),
  validate_fields
], post_user);

router.put('/:user_id', [
  check('user_id', 'no es un ID válido').isMongoId(),
  check('user_id').custom(validate_user_id_exists),
  check('role').custom(validate_role),
  validate_fields
], put_user);

router.patch('/', patch_user);

router.delete('/:user_id', [
  validate_jwt,
  validate_if_has_role('NOSE_ROLE'),
  // validate_admin_role,
  check('user_id', 'no es un ID válido').isMongoId(),
  check('user_id').custom(validate_user_id_exists),
  validate_fields
], delete_user);

module.exports = router;