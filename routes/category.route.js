const { Router } = require('express');
const { check } = require('express-validator');
const { validate_jwt, validate_admin_role } = require('../middlewares');
const { validate_fields } = require('../middlewares/validate_fields');
const { post_category, get_categories, get_category, put_category, delete_category } = require('../controllers/category.controller');
const { validate_category_id_exists } = require('../helpers/db_validators');
const router = Router();

router.get('/', get_categories);

router.get('/:category_id', [
  check('category_id', 'no es un ID válido').isMongoId(),
  check('category_id').custom(validate_category_id_exists),
  validate_fields
], get_category);

router.post('/', [
  check('name', 'El nombre es obligatorio').not().isEmpty(),
  validate_fields,
  validate_jwt,
], post_category);

router.put('/:category_id', [
  validate_jwt,
  check('name', 'El nombre es obligatorio').not().isEmpty(),
  check('category_id', 'no es un ID válido').isMongoId(),
  check('category_id').custom(validate_category_id_exists),
  validate_fields
], put_category);

router.delete('/:category_id', [
  validate_jwt,
  validate_admin_role,
  check('category_id', 'no es un ID válido').isMongoId(),
  check('category_id').custom(validate_category_id_exists),
  validate_fields
], delete_category);

module.exports = router;