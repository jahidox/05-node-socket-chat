const jwt = require('jsonwebtoken');
const User = require('../models/user.model');

const validate_admin_role = async (req, res, next) => {
  if (!req.user){
    return res.status(500).json({
      msg: 'Se quiere validar el rol sin validar primero el token'
    });
  }

  const {role, name} = req.user;
  if (role != 'ADMIN_ROLE') {
    return res.json({
        ok: false,
        err: {
            message: `${name} no es administrador`
        }
    });
  }

  next();
}

const validate_if_has_role = (...roles) => {
  return (req, res, next) => {
    if (!req.user){
      return res.status(500).json({
        msg: 'Se quiere validar el rol sin validar primero el token'
      });
    }

    if (!roles.includes(req.user.role)){
      return res.status(401).json({
        msg: `El servicio requiere uno de estos roles: ${roles}`
      });
    }
  }
}

module.exports = {
  validate_admin_role,
  validate_if_has_role
}