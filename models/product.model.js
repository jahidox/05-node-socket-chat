const { Schema, model } = require('mongoose');

let product_schema = Schema({
  name: {
    type: String,
    required: [true, 'Nombre obligatorio']
  },
  description: {
    type: String
  },
  available: {
    type: Boolean,
    default: true
  },
  state: {
    type: Boolean,
    default: true,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: 'Category',
    required: true
  },
  price: {
    type: Number,
    default: 0
  },
  img: {
    type: String,
    required: false
  },
});

product_schema.methods.toJSON = function() {
  const { __v, state, ...product } = this.toObject();
  return product;
}

module.exports = model('Product', product_schema);