const { response } = require('express');
const { Product } = require('../models');

const get_products = async (req, res = response) => {
  const { limit=5, since=0 } = req.query;

  const [total, products] = await Promise.all([
    Product.countDocuments({state:true}),
    Product.find({state:true})
      .populate('user', 'name')
      .populate('category', 'name')
      .skip(Number(since))
      .limit(Number(limit))
  ]);

  res.json({
    total,
    products
  });
};

const get_product = async (req, res = response) => {
  const { product_id } = req.params;

  const product = await Product.findById(product_id)
    .populate('user', 'name')
    .populate('category', 'name');

  res.json({
    product
  });
};

const post_product = async (req, res = response) => {
  const { state, user, ...data } = req.body;
  const is_product_exists = await Product.findOne({name: data.name.toUpperCase()});
  if (is_product_exists) {
    return res.status(400).json({
      msg: `El producto: ${data.name}, ya existe`
    });
  }
  const product = new Product({category: data.category, name: data.name.toUpperCase(), user: req.user._id});
  await product.save();

  res.status(201).json({
    product
  });
};

const put_product = async (req, res= response) => {
  const { product_id } = req.params;
  const { state, user, ...data} = req.body;

  if(data.name){
    data.name = data.name.toUpperCase();
  }

  data.user = req.user._id;

  const product = await Product.findByIdAndUpdate(product_id, data, {new: true})
    .populate('user', 'name')
    .populate('category', 'name');

  res.json({
    product
  });
};

const delete_product = async (req, res= response) => {
  const { product_id } = req.params;
  const product = await Product.findByIdAndUpdate(product_id, {state:false}, {new: true});

  res.json({
    product
  });
};

module.exports = {
  get_products,
  get_product,
  post_product,
  put_product,
  delete_product
}
