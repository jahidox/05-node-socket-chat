const { response } = require('express');
const Category = require('../models/category.model');

const get_categories = async (req, res = response) => {
  const { limit=5, since=0 } = req.query;

  const [total, categories] = await Promise.all([
    Category.countDocuments({state:true}),
    Category.find({state:true})
      .populate('user', 'name')
      .skip(Number(since))
      .limit(Number(limit))
  ]);

  res.json({
    total,
    categories
  });
};

const get_category = async (req, res = response) => {
  const { category_id } = req.params;

  const category = await Category.findById(category_id)
    .populate('user', 'name');

  res.json({
    category
  });
};

const post_category = async (req, res = response) => {
  const name = req.body.name.toUpperCase();
  const is_category_exists = await Category.findOne({name});
  if (is_category_exists) {
    return res.status(400).json({
      msg: `La categoria: ${name} ya existe`
    });
  }
  const category = new Category({name, user: req.user._id});

  await category.save();

  res.status(201).json({
    category
  });
};

const put_category = async (req, res= response) => {
  const { category_id } = req.params;
  const { state, user, ...data} = req.body;

  data.name = data.name.toUpperCase();
  data.user = req.user._id;

  const category = await Category.findByIdAndUpdate(category_id, data, {new: true})
    .populate('user', 'name');

  res.json({
    category
  });
};

const delete_category = async (req, res= response) => {
  const { category_id } = req.params;
  const category = await Category.findByIdAndUpdate(category_id, {state:false}, {new: true});

  res.json({
    category
  });
};

module.exports = {
  get_categories,
  get_category,
  post_category,
  put_category,
  delete_category
}
